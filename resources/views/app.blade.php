<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ isset($title) ? $title . ' - ' : null }}Laravel - The PHP Framework For Web Artisans</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="author" content="Taylor Otwell">
    <meta name="description" content="Laravel - The PHP framework for web artisans.">
    <meta name="keywords" content="laravel, php, framework, web, artisans, taylor otwell">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if (isset($canonical))
        <link rel="canonical" href="{{ url($canonical) }}" />
    @endif
    <link href='https://fonts.googleapis.com/css?family=Miriam+Libre:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ elixir('assets/css/laravel.css') }}">
    <link rel="stylesheet" href="/assets/js/static/flexboxgrid.6.3.1.min.css">
    <link rel="apple-touch-icon" href="/favicon.png">
    <script src="/assets/js/static/vuejs.1.0.26.min.js"></script>
</head>
<body class="@yield('body-class', 'docs') language-php">
    <span class="overlay"></span>

    <nav class="main">
        <a href="/" class="brand nav-block">
            <img src="/img/material-admin-logo.png">
        </a>

        <div class="search nav-block invisible">

        </div>

        <ul class="main-nav" v-if="! search">
            @include('partials.main-nav')
        </ul>

        @if (Request::is('docs*') && isset($currentVersion))
            @include('partials.switcher')
        @endif

        <div class="responsive-sidebar-nav">
            <a href="#" class="toggle-slide menu-link btn">&#9776;</a>
        </div>
    </nav>

    @yield('content')

    <footer class="main">
        <ul>
            @include('partials.main-nav')
        </ul>
        <p>Laravel is a trademark of Taylor Otwell. Copyright &copy; Taylor Otwell.</p>
        <p class="less-significant">
            <a href="http://jackmcdade.com">
                Designed by<br>
                {!! svg('jack-mcdade') !!}
            </a>
        </p>
    </footer>

    <script>
        var version             = 'xpto';
        {{--var version             = 'xpto';//'{{ isset($currentVersion) ? $currentVersion : DEFAULT_VERSION }}';--}}
    </script>

    <script src="{{ elixir('assets/js/laravel.js') }}"></script>
    <script src="/assets/js/viewport-units-buggyfill.js"></script>
    <script>window.viewportUnitsBuggyfill.init();</script>
    <script>
    </script>
</body>
</html>
