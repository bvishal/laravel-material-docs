# Installation

- [Introduction](#introduction) 
- [Installation](#installation) 
    - [Windows](#windows)
    - [Ubuntu](#ubuntu)
    - [Mac OS X](#macosx)

<a name="introduction"></a>
## Introduction
Material Pro Admin is a popular open source WebApp template for admin dashboards and control panels.
Material Pro Admin is fully responsive HTML template, which is based on the CSS framework Bootstrap 4. 
It utilizes all of the Bootstrap components in its design and re-styles many commonly used plugins to
create a consistent design that can be used as a user interface for backend applications. 
Monster Admin is based on a modular design, which allows it to be easily customized and built upon.
This documentation will guide you through installing the template and exploring 
the various components that are bundled with the template.


<a name="windows"></a>
## Windows

<div class="content-list" markdown="1">
1. Download and install Node.js 8.x from [NodeJS](https://nodejs.org/en/)
2. Download and install Git from [Git](https://git-scm.com/downloads)
3. Download and install Yarn from [Yarn](https://yarnpkg.com/en/docs/install)
4. Logout from the system and login again.
5. Launch <code>cmd.exe</code> as administrator and run command <code>npm install --add-python-to-path='true' --global --production windows-build-tools</code>.
6. Download PHP from [PHP](https://windows.php.net/download)
7. Download and install Composer from [Composer](https://getcomposer.org/doc/00-intro.md#installation-windows)
8. Logout from the system and login again.
9. Open <code>cmd.exe</code> and go to the <code>laravel-starter</code> directory.
10. Run command <code>composer install</code>.
11. Rename <code>.env.example</code> file to <code>.env</code> by running <code>ren .env.example .env</code>.
12. Run command <code>php artisan key:generate</code>.
13. Run command <code>yarn install</code>.
14. Run command <code>npm run dev</code>.
15. Now you can run the server: <code>php artisan serve</code>
</div>


<a name="ubuntu"></a>
## Ubuntu

<div class="content-list" markdown="1">
1. Upgrade system: <code>sudo apt-get update && sudo apt-get upgrade</code>.
2. Install curl: <code>sudo apt-get install curl</code>.
3. Install Node.js 8.x and build tools following instructions on [NodeJS](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions)
4. Install Git following instructions on [Git](https://git-scm.com/download/linux)
5. Install Yarn following instructions on [Yarn](https://yarnpkg.com/en/docs/install#debian-stable)
6. Open a terminal and install PHP: <code>sudo add-apt-repository ppa:ondrej/php; sudo apt-get update; sudo apt-get install php7.2 php-pear php7.2-curl php7.2-dev php7.2-gd php7.2-mbstring php7.2-zip php7.2-mysql php7.2-xml</code>.
7. Install Composer following instructions on [Composer](https://getcomposer.org/download/)
8. Make composer available globally following instructions on [GetComposer](https://getcomposer.org/doc/00-intro.md#globally)
9. Go to the <code>laravel-starter</code> directory.
10. Run command <code>composer install</code>.
11. Rename <code>.env.example</code> file to <code>.env</code> by running <code>ren .env.example .env</code>.
12. Run command <code>php artisan key:generate</code>.
13. Run command <code>yarn install</code>.
14. Run command <code>npm run dev</code>.
15. Now you can run the server: <code>php artisan serve</code>
</div>

<a name="macosx"></a>
## Mac OS X

<div class="content-list" markdown="1">
1. Install Xcode from App Store. After installing, launch the Xcode, accept the license agreement and wait while components installed.
2. Install Homebrew following instructions on [Homebrew](https://brew.sh)
3. Download and install Node.js 8.x from [NodeJS](https://nodejs.org/en/)
4. Download and install Git from [Git](https://git-scm.com/downloads)
5. Install Yarn following instructions on [Yarn](https://yarnpkg.com/en/docs/install)
6. Open a terminal.
7. Check PHP version <code>php -v</code>. If PHP is not installed or PHP version is less than <strong>7.1.3</strong>, run <code>brew install php</code>.
8. Install Composer following instructions on [Composer](https://getcomposer.org/download/)
9. Make composer available globally following instructions on [GetComposer](https://getcomposer.org/doc/00-intro.md#globally)
10. Run command <code>composer install</code>.</li>
11. Rename <code>.env.example</code> file to <code>.env</code> by running <code>ren .env.example .env</code>.
12. Run command <code>php artisan key:generate</code>.
13. Run command <code>yarn install</code>.
14. Run command <code>npm run dev</code>.
15. Now you can run the server: <code>php artisan serve</code>
</div>