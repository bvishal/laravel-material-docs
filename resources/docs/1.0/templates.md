# Templates

- [Introduction](#introduction)
- [Template Structure](#template-structure)
    - [Left Sidebar](#left-sidebar)
- [Templates List](#templates-list)
    - [Material](#material)
    - [Minisidebar](#minisidebar)
    - [Dark](#dark)
    - [Horizontal](#horizontal)
    - [Material-Rtl](#material-rtl)
    
<a name="introduction"></a>
## Introduction

Material Pro provides multiple templates to customize the entire WebApp. We provide a total of <b>5</b> different full customizable templates.

<a name="template-structure"></a>
## Template Structure
All template files have fixed structure consisting of header, mega menu, top menu, user profile, sidebar menu, right sidebar, content and footer as shown below:
<img src="/img/template-structure.jpg" style="width:100%" />

<a name="left-sidebar"></a>
### Left Sidebar

We have created leftsidebar with the following code.

        <!-- Stored in resources/views/templates/material/left-sidebar.blade.php -->    
        
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                @include('templates.application.components.sidebar-profile')
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
        
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Homes </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/material/home-one') }}">Home 1</a></li>
                                <li><a href="{{ url('/material/home-two') }}">Home 2</a></li>
                            </ul>
                        </li>
        
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/material/dashboard') }}">Dashboard 1</a></li>
                            </ul>
                        </li>
        
                        <li>
                            <a href="{{ url('/material/blade-view') }}" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu">Blade View </span></a>
                        </li>
        
                        <li>
                            <a href="{{ url('/material/components') }}" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">Components </span></a>
                        </li>
        
                        <li class="nav-small-cap">A SEPARATOR, OR TITLE</li>
        
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu"> Layouts </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('material.demos.boxed') }}">Boxed</a></li>
                                <li><a href="{{ route('material.demos.logocenter') }}">Logo Center</a></li>
                                <li><a href="{{ route('material.demos.singlecolumn') }}">Single Column</a></li>
                                <li><a href="{{ route('material.demos.fixheader') }}">Fix header</a></li>
                                <li><a href="{{ route('material.demos.fixsidebar') }}">Fix sidebar</a></li>
                                <li><a href="{{ route('material.demos.fixheadersidebar') }}">Fix header & Sidebar</a></li>
                            </ul>
                        </li>
        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item--><a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                <!-- item--><a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <!-- item--><a href="" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->

> {note} This is an example of the material left side bar. Other templates use a different layout for the sidebar. 


<a name="templates-list"></a>
## Templates List

In order to select a template, use the Blade `@extends` directive to specify which template the view should "inherit".

<a name="material"></a>
### Material

    <!-- Stored in resources/views/demo-apps/material/home.blade.php -->

    @extends('templates.material.main')
    
    @section('content')
    
        <div class="card">
            <div class="card-body">
                <h4 class="font-weight-bold py-3 mb-4">Home One</h4>
                <p>This page is an example of basic layout to get you started.</p>
                <p><button class="btn btn-primary btn-lg">Button</button></p>
            </div>
        </div>
    
        @include('demo-content.card-long-text')
    
    @endsection


<a name="minisidebar"></a>
### Minisidebar

    <!-- Stored in resources/views/demo-apps/minisidebar/home.blade.php -->

    @extends('templates.minisidebar.main')
    
    @section('content')
    
        <div class="card">
            <div class="card-body">
                <h4 class="font-weight-bold py-3 mb-4">Home One</h4>
                <p>This page is an example of basic layout to get you started.</p>
                <p><button class="btn btn-primary btn-lg">Button</button></p>
            </div>
        </div>
    
        @include('demo-content.card-long-text')
    
    @endsection


<a name="dark"></a>
### Dark

    <!-- Stored in resources/views/demo-apps/dark/home.blade.php -->

    @extends('templates.dark.main')
    
    @section('content')
    
        <div class="card">
            <div class="card-body">
                <h4 class="font-weight-bold py-3 mb-4">Home One</h4>
                <p>This page is an example of basic layout to get you started.</p>
                <p><button class="btn btn-primary btn-lg">Button</button></p>
            </div>
        </div>
    
    @endsection


<a name="horizontal"></a>
### Horizontal

    <!-- Stored in resources/views/demo-apps/horizontal/home.blade.php -->

    @extends('templates.horizontal.main')
    
    @section('content')
    
        <div class="card">
            <div class="card-body">
                <h4 class="font-weight-bold py-3 mb-4">Home One</h4>
                <p>This page is an example of basic layout to get you started.</p>
                <p><button class="btn btn-primary btn-lg">Button</button></p>
            </div>
        </div>
    
        @include('demo-content.card-long-text')
    
    @endsection


<a name="material-rtl"></a>
### Material Rtl

    <!-- Stored in resources/views/demo-apps/material-rtl/home.blade.php -->

    @extends('templates.material-rtl.main')
    
    @section('content')
    
        <div class="card">
            <div class="card-body">
                <h4 class="font-weight-bold py-3 mb-4">Home One</h4>
                <p>This page is an example of basic layout to get you started.</p>
                <p><button class="btn btn-primary btn-lg">Button</button></p>
            </div>
        </div>
    
        @include('demo-content.card-long-text')
    
    @endsection
