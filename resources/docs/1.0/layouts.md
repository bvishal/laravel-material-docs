# Layouts

- [Introduction](#introduction)
- [Layouts List](#layouts-list)
    - [Fix Header](#fix-header)
    - [Fix Sidebar](#fix-sidebar)
    - [Boxed](#boxed)
    - [Logo Center](#logo-center)
    - [Single Column](#single-column)
    
<a name="introduction"></a>
## Introduction

Material Pro provides multiple layouts to customize the selected template. We have made total 5 layout variation.
The layout you select will be placed in the `body` tag. 
     
<a name="layouts-list"></a>
## Layouts List

In order to select a layout, use the Blade `@section` directive to specify which layout the view should have.

<a name="fix-header"></a>
### Fix Header

<img src="/img/fix-header.jpg" style="width:100%" />

    <!-- Stored in resources/views/demo-apps/material/fix-header.blade.php -->

    @extends('templates.material.main')
    
    {{--Attributes for Layout are added here --}}
    @section('body-classes','fix-header')
    
    @section('content')
    
        @include('demo-content.fix-header')
    
    @endsection

<a name="fix-sidebar"></a>
### Fix Sidebar

<img src="/img/fix-sidebar.jpg" style="width:100%" />

    <!-- Stored in resources/views/demo-apps/material/fix-sidebar.blade.php -->
    
    @extends('templates.material.main')
    
    {{--Attributes for Layout are added here --}}
    @section('body-classes','fix-sidebar')
    
    @section('content')
    
        @include('demo-content.fix-sidebar')
    
    @endsection
    
    
> {tip} You can use Fix Header and Fix Sidebar with other layouts. Example: ` @section('body-classes','fix-sidebar boxed')`


<a name="boxed"></a>
### Boxed

<img src="/img/home-boxed.jpg" style="width:100%" />

    <!-- Stored in resources/views/demo-apps/material/home-boxed.blade.php -->

    @extends('templates.material.main')
    
    {{--Attributes for Layout are added here --}}
    @section('body-classes','boxed')
    
    @section('content')
    
        @include('demo-content.home-boxed')
    
    @endsection

<a name="logo-center"></a>
### Logo Center

<img src="/img/logo-center.jpg" style="width:100%" />

    <!-- Stored in resources/views/demo-apps/material/home.blade.php -->

      @extends('templates.material.main')
      
      {{--Attributes for Layout are added here --}}
      @section('body-classes','logo-center')
      
      @section('content')
      
          @include('demo-content.logo-center')
      
      @endsection

<a name="single-column"></a>
### Single Colun

<img src="/img/single-column.jpg" style="width:100%" />

    <!-- Stored in resources/views/demo-apps/material/single-column.blade.php -->

    @extends('templates.material.main')
    
    {{--Attributes for Layout are added here --}}
    @section('body-classes','single-column')
    
    @section('content')
    
        @include('demo-content.single-column')
    
    @endsection
