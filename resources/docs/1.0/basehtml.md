# Base HTML

- [Introduction](#introduction)
- [Base HTML](#base-html)
    - [Defining The Base](#defining-the-base)
    - [Extending The Base](#extending-the-base)
    
<a name="introduction"></a>
## Introduction

Blade is the simple, yet powerful templating engine provided with Laravel. Unlike other popular PHP templating engines, Blade does not restrict you from using plain PHP code in your views. In fact, all Blade views are compiled into plain PHP code and cached until they are modified, meaning Blade adds essentially zero overhead to your application. Blade view files use the `.blade.php` file extension and are typically stored in the `resources/views` directory.

<a name="base-html"></a>
## Base HTML

<a name="defining-the-base"></a>
### Defining The Base
This is the HTML base of Material Pro. It contains all the information and sections to customize the template. All of the information within the page content area is nested within a div with id of "main-wrapper".
    
    <!-- Stored in resources/views/templates/application/master.blade.php -->

    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="/images/vendor/wrappixel/material-pro/4.1.0/assets/images/favicon.png">
        {{--<link rel="icon" type="image/png" sizes="16x16" href="/vendor/wrappixel/material-pro/4.1.0/assets/images/favicon.png">--}}
        <title>Material Pro Admin Template - The Most Complete & Trusted Bootstrap 4 Admin Template</title>
    
        @stack('before-styles')
    
        <!-- Bootstrap Core CSS -->
        <link href="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Template CSS -->
    
        @section('template-css')
        {{--Defaults to Material and Blue--}}
    
        {{-- ### Choose only the one you want ### --}}
        <link href="/vendor/wrappixel/material-pro/4.1.0/material/css/style.css" rel="stylesheet">
        {{--<link href="/vendor/wrappixel/material-pro/4.1.0/dark/css/style.css" rel="stylesheet">--}}
        {{--<link href="/vendor/wrappixel/material-pro/4.1.0/minisidebar/css/style.css" rel="stylesheet">--}}
        {{--<link href="/vendor/wrappixel/material-pro/4.1.0/horizontal/css/style.css" rel="stylesheet">--}}
        {{--<link href="/vendor/wrappixel/material-pro/4.1.0/material-rtl/css/style.css" rel="stylesheet">--}}
    
        <!-- You can change the theme colors from here -->
        <link href="/css/colors/blue.css" id="theme" rel="stylesheet">
        @show
    
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="/vendor/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="/vendor/respondjs/1.4.2/respond.min.js"></script>
        <![endif]-->
    
        @stack('after-styles')
    
    </head>
    
    <body class=" @yield('body-classes') card-no-border ">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    
    <div id="main-wrapper">
    @yield('layout-content')
    </div>
    
    @stack('before-scripts')
    
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/popper/popper.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/material/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/material/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/material/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    @section('template-custom-js')
        {{--Defaults to Material --}}
    
        {{-- ### Choose only the one you want ### --}}
        <script src="/vendor/wrappixel/material-pro/4.1.0/material/js/custom.min.js"></script>
        {{--<script src="/vendor/wrappixel/material-pro/4.1.0/dark/js/custom.min.js"></script>--}}
        {{--<script src="/vendor/wrappixel/material-pro/4.1.0/minisidebar/js/custom.min.js"></script>--}}
        {{--<script src="/vendor/wrappixel/material-pro/4.1.0/horizontal/js/custom.min.js"></script>--}}
        {{--<script src="/vendor/wrappixel/material-pro/4.1.0/material-rtl/js/custom.min.js"></script>--}}
    @show
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="/vendor/wrappixel/material-pro/4.1.0/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    
    
    @stack('after-scripts')
    
    
    </body>
    
    </html>

As you can see, this file contains typical HTML mark-up. However, take note of the `@section` and `@yield` directives. The `@section` directive, as the name implies, defines a section of content, while the `@yield` directive is used to display the contents of a given section.

Now that we have defined a layout for our application, let's define a child page that inherits the layout.

> {tip} You can select the color theme by replacing `/css/colors/blue.css` with the desired color . Example: `/css/colors/green.css`

<a name="extending-the-base"></a>
### Extending The Base

When defining a child view, use the Blade `@extends` directive to specify which layout the child view should "inherit". Views which extend a Blade layout may inject content into the layout's sections using `@section` directives. Remember, as seen in the example above, the contents of these sections will be displayed in the layout using `@yield`:

    <!-- Stored in resources/views/templates/material/main.blade.php -->

    @extends('templates.application.master')
    
    {{-- ### Attributes for Layout are added here ### --}}
    {{--Possibilities:  'fix-header'  'fix-sidebar' 'boxed' 'logo-center' 'single-column' --}}
    {{--You can make combinations with them--}}
    @section('body-classes','')
    
    @section('template-css')
        <link href="/vendor/wrappixel/material-pro/4.1.0/material/css/style.css" rel="stylesheet">
        <link href="/css/colors/blue.css" id="theme" rel="stylesheet">
    @endsection
    
    @section('template-custom-js')
        <script src="/vendor/wrappixel/material-pro/4.1.0/material/js/custom.min.js"></script>
    @endsection
    
    @section('layout-content')
    
        @include('templates.application.includes.topbar')
    
        @include('templates.material.left-sidebar')
    
        <div class="page-wrapper">
    
            <div class="container-fluid">
    
                @if(true)
                    @include('templates.application.includes.breadcrumb')
                @else
                    <div class="row mb-4"></div>
                @endif
    
                @yield('content')
    
                @include('templates.application.includes.right-sidebar')
    
            </div>
    
        </div>
    
    @endsection