- ## Guetting Started
    - [Installation](/docs/{{version}}/installation)

- ## Structures
    - [Files Structure](/docs/{{version}}/structure)

- ## Frontend
    - [Base HTML](/docs/{{version}}/basehtml)
    - [Color Variations](/docs/{{version}}/colorvariations)
    - [Templates](/docs/{{version}}/templates)
    - [Layouts](/docs/{{version}}/layouts)
    - [Components](/docs/{{version}}/components)
    - [Compiling Assets](/docs/{{version}}/mix)