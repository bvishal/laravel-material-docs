# Color Variations

- [Introduction](#introduction)
- [Color Variations](#color-variations)
    - [Light Sidebar](#light-sidebar)
        - [Default](#default)
        - [Green](#green)
        - [Red](#red)
        - [Blue](#blue)
        - [Purple](#purple)
        - [Megna](#megna)
    - [Dark Sidebar](#dark-sidebar)
        - [Default](#default-dark)
        - [Green](#green-dark)
        - [Red](#red-dark)
        - [Blue](#blue-dark)
        - [Purple](#purple-dark)
        - [Megna](#megna-dark)
    
<a name="introduction"></a>
## Introduction

We have made a total of <b>6</b> color variation with light and dark sidebar.
<img src="/img/colors.jpg" style="width:100%" />

<a name="color-variations"></a>
## Color Variations

To change the teme colors, just place the following css file in to the `head`.

<a name="light-sidebar"></a>
### Light Sidebar

<a name="default"></a>
#### Default

    <link href="/css/colors/default.css" id="theme" rel="stylesheet">

<a name="green"></a>
#### Green

    <link href="/css/colors/green.css" id="theme" rel="stylesheet">

<a name="red"></a>
#### Red

    <link href="/css/colors/red.css" id="theme" rel="stylesheet">

<a name="blue"></a>
#### Blue

    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">

<a name="purple"></a>
#### Purple

    <link href="/css/colors/purple.css" id="theme" rel="stylesheet">

<a name="megna"></a>
#### Megna

    <link href="/css/colors/megna.css" id="theme" rel="stylesheet">

<a name="dark-sidebar"></a>
### Dark Sidebar

<a name="default-dark"></a>
#### Default

    <link href="/css/colors/default-dark.css" id="theme" rel="stylesheet">

<a name="green-dark"></a>
#### Green

    <link href="/css/colors/green-dark.css" id="theme" rel="stylesheet">

<a name="red-dark"></a>
#### Red

    <link href="/css/colors/red-dark.css" id="theme" rel="stylesheet">

<a name="blue-dark"></a>
#### Blue

    <link href="/css/colors/blue-dark.css" id="theme" rel="stylesheet">

<a name="purple-dark"></a>
#### Purple

    <link href="/css/colors/purple-dark.css" id="theme" rel="stylesheet">

<a name="megna-dark"></a>
#### Megna

    <link href="/css/colors/megna-dark.css" id="theme" rel="stylesheet">