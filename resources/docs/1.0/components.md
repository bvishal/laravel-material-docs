# Components

- [Introduction](#introduction)
- [Components](#components)
    - [Button](#button)
    - [Dropdown Button](#dropdown-button)
    - [Tab](#tab)
    - [Ui Card](#ui-card)
    - [Ui User Card](#ui-user-card)
    
<a name="introduction"></a>
## Introduction

Components and slots provide similar benefits to sections and layouts; however, some may find the mental model of components and slots easier to understand. 
Material Pro provides multiple components to allow you to reuse throughout your application.
The code behind the componets is stored in `views/component-library/ui-elements`.

<a name="template-structure"></a>
## Components

<a name="button"></a>
### Button

<img src="/img/buttons.jpg"  />

    <!-- Stored in resources/views/demo-content/components.blade.php -->    
    @component('component-library.ui-elements.button',
    [
         'color' => 'btn-secondary',
         'text'  => 'Secondary',
    ])@endcomponent
     
<a name="dropdown-button"></a>
### Dropdown Button
  
     <!-- Stored in resources/views/demo-content/components.blade.php -->  
     @component('component-library.ui-elements.dropdown-button',
             [
                 'text'   => 'Go somewhere',
                 'class'   => 'btn-secondary',
                 'sr_text'   => 'Go somewhere'
             ])
     @endcomponent
     
<a name="tab"></a>
### Tab
<img src="/img/tabs.jpg"  />

     <!-- Stored in resources/views/demo-content/components.blade.php -->    
     @component('component-library.ui-elements.tab',
     [
         'tab' => $tabs['defaultTabs'],
         'tabContentBorder' => true,
     ])@endcomponent

<a name="ui-card"></a>
### Ui Card
<img src="/img/ui-cards.jpg"  />

    <!-- Stored in resources/views/demo-content/components.blade.php -->    
    @component('component-library.ui-elements.ui-card',
    [
        'url'           => '/images/vendor/wrappixel/material-pro/4.1.0/assets/images/big/img1.jpg',
        'alt'           => 'Card image cap',
        'title'         => 'Card title',
        'action'        => '#',
        'text'          => "Some quick example text to build on the card title and make up the bulk of the card's content.",
        'button_text'   => 'Go somewhere',
    ])
    @endcomponent

<a name="ui-user-card"></a>
### Ui User Card
<img src="/img/ui-user-cards.jpg"  />

    <!-- Stored in resources/views/demo-content/components.blade.php -->    
    @component('component-library.ui-elements.ui-user-card',
    [
            'url'       => '/images/vendor/wrappixel/material-pro/4.1.0/assets/images/users/1.jpg',
            'alt'       => 'user',
            'action'    => '#',
            'title'     => 'Genelia Deshmukh',
            'subtitle'  => 'Managing Director',
    ])
    @endcomponent